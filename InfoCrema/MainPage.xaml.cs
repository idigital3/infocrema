﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfoCrema.Models;
using InfoCrema.Utility;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace InfoCrema
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            var tapGesture = new Xamarin.Forms.TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;

            stack_bike.GestureRecognizers.Add(tapGesture);
            stack_car.GestureRecognizers.Add(tapGesture);
            stack_food.GestureRecognizers.Add(tapGesture);
            stack_hotel.GestureRecognizers.Add(tapGesture);
            stack_restaurant.GestureRecognizers.Add(tapGesture);
            stack_product.GestureRecognizers.Add(tapGesture);
            stack_wine.GestureRecognizers.Add(tapGesture);

            Sistema.PopolaSezioni();
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
        }



        private async void TapGesture_Tapped(object sender, EventArgs e)
        {
            string nome_componente = sender.GetType().Name;

            if (nome_componente.Equals("StackLayout"))
            {
                StackLayout stack = (StackLayout)sender;
                string class_name = stack.ClassId;

                if (class_name.Equals("stack_bike"))
                    await Navigation.PushAsync(new Dettaglio(Sistema.Sezione.Bike));
                else if (class_name.Equals("stack_car"))
                    await Navigation.PushAsync(new Dettaglio(Sistema.Sezione.Car));
                else if (class_name.Equals("stack_food"))
                    await Navigation.PushAsync(new Dettaglio(Sistema.Sezione.Food));
                else if (class_name.Equals("stack_hotel"))
                    await Navigation.PushAsync(new Dettaglio(Sistema.Sezione.Hotel));
                else if (class_name.Equals("stack_restaurant"))
                    await Navigation.PushAsync(new Dettaglio(Sistema.Sezione.Restaurant));
                else if (class_name.Equals("stack_product"))
                    await Navigation.PushAsync(new Dettaglio(Sistema.Sezione.Product));
                else if (class_name.Equals("stack_wine"))
                    await Navigation.PushAsync(new Dettaglio(Sistema.Sezione.Wine));

            }
        }
    }
}
