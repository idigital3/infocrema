﻿using System;
using System.Threading.Tasks;
using Refit;
using System.Collections.Generic;

namespace InfoCrema.Services
{
    public class RestApi
    {
        public const string BASE_URL_REST_API = "";
        internal static IRestApi RefitApi { get; private set; }

        static RestApi()
        {
            RefitApi = Refit.RestService.For<IRestApi>(BASE_URL_REST_API);
        }

        /*
        internal static async Task<RootObject> GetData()
        => await RefitApi.GetData();

        internal static async Task<Response> CheckToken(string token)
        => await RefitApi.CheckToken(token);
        */
    }
}
