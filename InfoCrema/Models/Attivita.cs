﻿using System;
using System.Collections.Generic;
using InfoCrema.Utility;

namespace InfoCrema.Models
{
    public class Attivita
    {
        public Sistema.Sezione Sezione { get; set; }
        public string Nome { get; set; }
        public string Indirizzo { get; set; }
        public string Telefono { get; set; }
        public string SitoWeb { get; set; }
        public string Email { get; set; }
        public List<Immagine> Immagini { get; set; }
        public string Descrizione { get; set; }
        public string Latitudine { get; set; }
        public string Longitudine { get; set; }
        public List<string> Codici { get; set; }
    }
}
