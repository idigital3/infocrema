﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace InfoCrema.Controlli
{
    public partial class Loading : ContentView
    {
        public Loading()
        {
            InitializeComponent();

            if (Device.OS == TargetPlatform.Android)
            {

                frame.CornerRadius = 3;
                progress.Margin = new Thickness(0, 3, 0, 0);
                label.Margin = new Thickness(0, 0, 0, 0);
            }
        }
    }
}
