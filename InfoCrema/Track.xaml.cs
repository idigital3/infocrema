﻿using System;
using System.Collections.Generic;
using System.Linq;
using InfoCrema.Models;
using InfoCrema.Utility;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace InfoCrema
{
    public partial class Track : ContentPage
    {
        Attivita attivita;
        string track;
        public Track(Attivita attivita,string track)
        {
            InitializeComponent();

            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            this.attivita = attivita;
            this.track = track;

            l_titolo.Text = "Bike Rental";
            l_sottotitolo.Text = track;

            InitMappa();
        }

        public Track(){


        }



        async void InitMappa()
        {
            string nomeFile = "";
            if (track.ToLower() == "track 1")
                nomeFile = "Escursione_pomeridiana.gpx";
            else if (track.ToLower() == "track 2")
            {
                nomeFile = "";
            }
            else if (track.ToLower() == "track 3")
            {
                nomeFile = "";
            }
            else if (track.ToLower() == "track 4")
            {
                nomeFile = "";
            }



            List<Position> listaPosizioni = Sistema.GetPosizioni(nomeFile);

            if(listaPosizioni.Count > 0)
            {
                var customMap = new CustomMap
                {
                    MapType = MapType.Satellite,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    HasZoomEnabled = true,
                    IsShowingUser = true
                };

                var pinStart = new Pin();
                pinStart.Type = PinType.Place;
                pinStart.Position = new Position(listaPosizioni.First().Latitude, listaPosizioni.First().Longitude);
                pinStart.Label = "START";

                var pinFinish = new Pin();
                pinFinish.Type = PinType.Place;
                pinFinish.Position = new Position(listaPosizioni.Last().Latitude, listaPosizioni.Last().Longitude);
                pinFinish.Label = "FINISH";

                customMap.Pins.Add(pinStart);
                customMap.Pins.Add(pinFinish);

                stack_mappa.Children.Add(customMap);

                customMap.RouteCoordinates = listaPosizioni;

                customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(listaPosizioni[0].Latitude, listaPosizioni[0].Longitude), Distance.FromKilometers(1.0)));


                /*
                string puntiIntermedi = "";
                foreach (Position item in listaPosizioni)
                {
                    if (item == listaPosizioni.First() || item == listaPosizioni.Last())
                        continue;

                    puntiIntermedi += item.Latitude.ToString() + "," + item.Longitude.ToString()+"%7C";

                }


                string result = puntiIntermedi.Remove(puntiIntermedi.Length - 3, 3);

                var uri = new Uri("http://maps.google.com/maps/dir/?api=1&origin=" + listaPosizioni.First().Latitude.ToString() + "," + listaPosizioni.First().Longitude.ToString() + "&destination=" + listaPosizioni.Last().Latitude.ToString() + "," + listaPosizioni.Last().Longitude.ToString() + "&travelmode=bicycling");

                Device.OpenUri(uri);

                */
            }
        }
    }
}
