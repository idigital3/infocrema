﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using InfoCrema.Models;
using InfoCrema.Utility;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace InfoCrema
{
    public partial class Dettaglio : ContentPage
    {
        Sistema.Sezione sezione;
        Attivita attivita;
        public Dettaglio(Sistema.Sezione sezione)
        {
            InitializeComponent();

            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            this.sezione = sezione;

            var tapGesture = new Xamarin.Forms.TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;

            stack_telefono.GestureRecognizers.Add(tapGesture);
            stack_email.GestureRecognizers.Add(tapGesture);
            stack_sito.GestureRecognizers.Add(tapGesture);

            btn_navigatore.Clicked += Btn_Navigatore_Clicked;
            btn_tracciato.Clicked += Btn_Tracciato_Clicked;



            Init();
            InitMappa();
        }



        public Dettaglio()
        {

        }



        private void Init()
        {
            attivita = Sistema.listaAttivita.FirstOrDefault(x => x.Sezione == sezione);

            l_titolo.Text = attivita.Nome;
            l_indirizzo.Text = attivita.Indirizzo;
            l_telefono.Text = attivita.Telefono;
            l_sito.Text = attivita.SitoWeb;
            l_email.Text = attivita.Email;
            l_descrizione.Text = attivita.Descrizione;

            if (string.IsNullOrWhiteSpace(attivita.Descrizione))
                l_descrizione.IsVisible = false;

            if (sezione == Sistema.Sezione.Bike)
                btn_tracciato.IsVisible = true;


            if (attivita.Immagini != null && attivita.Immagini.Count > 0)
                carouselImmagini.ItemsSource = attivita.Immagini;


        }


        void Btn_Navigatore_Clicked(object sender, EventArgs e)
        {
            //var uri = new Uri("http://maps.google.com/maps?saddr=Google+Inc,+8th+Avenue,+New+York,+NY&daddr=John+F.+Kennedy+International+Airport,+Van+Wyck+Expressway,+Jamaica,+New+York&directionsmode=transit");

            var uri = new Uri("http://maps.google.com/maps?daddr="+attivita.Indirizzo.Replace(' ','+')+"&directionsmode=transit");
            Device.OpenUri(uri);

        }


        async void Btn_Tracciato_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListaTracciati());
        }



        void TapGesture_Tapped(object sender, EventArgs e)
        {
            string nome_componente = sender.GetType().Name;

            if (nome_componente.Equals("StackLayout"))
            {
                StackLayout stack = (StackLayout)sender;
                string class_name = stack.ClassId;

                if (class_name.Equals("stack_telefono") && attivita.Telefono != "-")
                    Device.OpenUri(new Uri("tel:" + attivita.Telefono.Replace(" ", string.Empty)));
                else if(class_name.Equals("stack_email") && attivita.Email != "-")
                    Device.OpenUri(new Uri("mailto:" + attivita.Email));
                else if (class_name.Equals("stack_sito") && attivita.SitoWeb != "-")
                    Device.OpenUri(new Uri("http://"+attivita.SitoWeb));


            }
        }




        void InitMappa()
        {
            Position position = new Position(Convert.ToDouble(attivita.Latitudine,CultureInfo.InvariantCulture), Convert.ToDouble(attivita.Longitudine,CultureInfo.InvariantCulture));
            var map = new Map
            {
                HeightRequest = 300,
                VerticalOptions = LayoutOptions.FillAndExpand,
                IsShowingUser = true,
                HasZoomEnabled = true
            };

            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = attivita.Nome
            };

            map.Pins.Add(pin);

            map.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromKilometers(0.50)));
            
            stack_mappa.Children.Add(map);
        }

    }
}
