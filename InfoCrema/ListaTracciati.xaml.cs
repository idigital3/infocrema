﻿using System;
using System.Collections.Generic;
using System.Linq;
using InfoCrema.Models;
using InfoCrema.Utility;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace InfoCrema
{
    public partial class ListaTracciati : ContentPage
    {
        Attivita attivita;
        public ListaTracciati()
        {
            InitializeComponent();

            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            var tapGesture = new Xamarin.Forms.TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;

            stack_track_1.GestureRecognizers.Add(tapGesture);
            stack_track_2.GestureRecognizers.Add(tapGesture);
            stack_track_3.GestureRecognizers.Add(tapGesture);
            stack_track_4.GestureRecognizers.Add(tapGesture);

            attivita = Sistema.listaAttivita.FirstOrDefault(x => x.Sezione == Sistema.Sezione.Bike);

            l_titolo.Text = "Bike Rental";

            popUpCodice.pageContent = this;
            popUpCodice.attivita = attivita;
        }



        async void TapGesture_Tapped(object sender, EventArgs e)
        {
            string nome_componente = sender.GetType().Name;

            if (nome_componente.Equals("StackLayout"))
            {
                StackLayout stack = (StackLayout)sender;
                string class_name = stack.ClassId;

                if(string.IsNullOrWhiteSpace(Sistema.CodiceBike))
                {
                    popUpCodice.IsVisible = true;

                    return;
                }

                if (class_name.Equals("stack_track_1"))
                    await Navigation.PushAsync(new Track(attivita,"Track 1"));
                else{
                    await DisplayAlert("Attenzione", "Tracciato non disponibile", "OK");
                    return;
                }
                /*else if (class_name.Equals("stack_track_2"))
                    await Navigation.PushAsync(new Track(attivita, "Track 2"));
                else if (class_name.Equals("stack_track_3"))
                    await Navigation.PushAsync(new Track(attivita, "Track 3"));
                else if (class_name.Equals("stack_track_4"))
                    await Navigation.PushAsync(new Track(attivita, "Track 4"));*/


            }
        }




        public async void InserisciCodiceCompleted(bool codiceValido,string messaggio,string codice)
        {
            if(codiceValido)
            {
                Sistema.CodiceBike = codice;

                await DisplayAlert("Complimenti", messaggio, "OK");
                return;
            }
            else
            {
                await DisplayAlert("Attenzione", messaggio, "OK");
                return;
            }

        }

    }
}
