﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using InfoCrema.Models;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace InfoCrema.Utility
{
    public class Sistema
    {
        public static string URL_DOMINIO = "";

        public enum Sezione { Bike, Car, Food, Hotel, Restaurant, Product, Wine };

        public static List<Attivita> listaAttivita;

        public Sistema()
        {
        }
        

        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }



        public static string CodiceBike
        {
            get => AppSettings.GetValueOrDefault(nameof(CodiceBike), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(CodiceBike), value);
        }



        public static List<Attivita> PopolaSezioni()
        {
            listaAttivita = new List<Attivita>();

            //VINO*************
            Attivita attivitaVino = new Attivita();
            attivitaVino.Sezione = Sezione.Wine;
            attivitaVino.Nome = "Vineria Fuoriporta";
            attivitaVino.Indirizzo = "Via Giacomo Matteotti, 15, 26013 Crema CR";
            attivitaVino.Telefono = "0373 83747";
            attivitaVino.SitoWeb = "www.vineriafuoriporta.it";
            attivitaVino.Email = "-";
            attivitaVino.Descrizione = "";
            attivitaVino.Latitudine = "45.362760";
            attivitaVino.Longitudine = "9.688400";

            List<Immagine> listaImmaginiVino = new List<Immagine>();

            listaImmaginiVino.Add(new Immagine { ImageUrl = "foto_vino_1.jpg"});
            listaImmaginiVino.Add(new Immagine { ImageUrl = "foto_vino_2.jpg" });
            listaImmaginiVino.Add(new Immagine { ImageUrl = "foto_vino_3.jpg" });
            listaImmaginiVino.Add(new Immagine { ImageUrl = "foto_vino_4.jpg" });

            attivitaVino.Immagini = listaImmaginiVino;

            listaAttivita.Add(attivitaVino);

            //BIKE**************
            Attivita attivitaBike = new Attivita();
            attivitaBike.Sezione = Sezione.Bike;
            attivitaBike.Nome = "Dal ciclista";
            attivitaBike.Indirizzo = "Via Luigi Cadorna, 33, 26013 Crema CR";
            attivitaBike.Telefono = " 0373 83862";
            attivitaBike.SitoWeb = "www.dalciclista.it/";
            attivitaBike.Email = "info@dalciclista.it";
            attivitaBike.Descrizione = "";
            attivitaBike.Latitudine = "45.363940";
            attivitaBike.Longitudine = "9.698050";

            List<Immagine> listaImmaginiBike = new List<Immagine>();

            listaImmaginiBike.Add(new Immagine { ImageUrl = "foto_bici_1.jpg" });
            listaImmaginiBike.Add(new Immagine { ImageUrl = "foto_bici_2.jpg" });
            listaImmaginiBike.Add(new Immagine { ImageUrl = "foto_bici_3.jpg" });
            listaImmaginiBike.Add(new Immagine { ImageUrl = "foto_bici_4.jpg" });

            attivitaBike.Immagini = listaImmaginiBike;

            List<string> listaCodici = new List<string>();
            listaCodici.Add("BR_1111");
            listaCodici.Add("BR_8871");
            listaCodici.Add("BR_9123");
            listaCodici.Add("BR_8712");
            listaCodici.Add("BR_3391");
            listaCodici.Add("BR_7771");
            listaCodici.Add("BR_3332");
            listaCodici.Add("BR_2171");
            listaCodici.Add("BR_4536");

            attivitaBike.Codici = listaCodici;

            listaAttivita.Add(attivitaBike);

            //HOTEL********************
            Attivita attivitaHotel = new Attivita();
            attivitaHotel.Sezione = Sezione.Hotel;
            attivitaHotel.Nome = "Il ponte di rialto";
            attivitaHotel.Indirizzo = "Via Luigi Cadorna, 7, 26013 Crema CR";
            attivitaHotel.Telefono = "0373 82342";
            attivitaHotel.SitoWeb = "www.pontedirialto.it";
            attivitaHotel.Email = "info@pontedirialto.it";
            attivitaHotel.Descrizione = "";
            attivitaHotel.Latitudine = "45.3640516";
            attivitaHotel.Longitudine = "9.695810700000038";

            List<Immagine> listaImmaginiHotel = new List<Immagine>();

            listaImmaginiHotel.Add(new Immagine { ImageUrl = "foto_hotel_1.jpg" });
            listaImmaginiHotel.Add(new Immagine { ImageUrl = "foto_hotel_2.jpg" });
            listaImmaginiHotel.Add(new Immagine { ImageUrl = "foto_hotel_3.jpg" });
            listaImmaginiHotel.Add(new Immagine { ImageUrl = "foto_hotel_4.jpg" });

            attivitaHotel.Immagini = listaImmaginiHotel;

            listaAttivita.Add(attivitaHotel);

            //RISTORANTE**********************
            Attivita attivitaRistorante = new Attivita();
            attivitaRistorante.Sezione = Sezione.Restaurant;
            attivitaRistorante.Nome = "Garage Cafè Retro";
            attivitaRistorante.Indirizzo = "Via Carlo Urbino, 26013 Crema CR";
            attivitaRistorante.Telefono = "3478276862";
            attivitaRistorante.SitoWeb = "-";
            attivitaRistorante.Email = "-";
            attivitaRistorante.Descrizione = "";
            attivitaRistorante.Latitudine = "45.3578163";
            attivitaRistorante.Longitudine = "9.684111600000051";

            List<Immagine> listaImmaginiRistorante = new List<Immagine>();

            listaImmaginiRistorante.Add(new Immagine { ImageUrl = "foto_ristorante_1.jpg" });
            listaImmaginiRistorante.Add(new Immagine { ImageUrl = "foto_ristorante_2.jpg" });
            listaImmaginiRistorante.Add(new Immagine { ImageUrl = "foto_ristorante_3.jpg" });
            listaImmaginiRistorante.Add(new Immagine { ImageUrl = "foto_ristorante_4.jpg" });

            attivitaRistorante.Immagini = listaImmaginiRistorante;

            listaAttivita.Add(attivitaRistorante);

            //FAST FOOD
            Attivita attivitaFastFood = new Attivita();
            attivitaFastFood.Sezione = Sezione.Food;
            attivitaFastFood.Nome = "Be Happy";
            attivitaFastFood.Indirizzo = "Via Francesco Crispi, 8, 26013 Crema CR";
            attivitaFastFood.Telefono = "3406265197";
            attivitaFastFood.SitoWeb = "-";
            attivitaFastFood.Email = "-";
            attivitaFastFood.Descrizione = "";
            attivitaFastFood.Latitudine = "45.36015010000001";
            attivitaFastFood.Longitudine = "9.682445199999961";

            List<Immagine> listaImmaginiFastFood = new List<Immagine>();

            listaImmaginiFastFood.Add(new Immagine { ImageUrl = "foto_fast_1.jpg" });
            listaImmaginiFastFood.Add(new Immagine { ImageUrl = "foto_fast_4.jpg" });
            listaImmaginiFastFood.Add(new Immagine { ImageUrl = "foto_fast_3.jpg" });
            //listaImmaginiFastFood.Add(new Immagine { ImageUrl = "foto_fast_2.jpg" });

            attivitaFastFood.Immagini = listaImmaginiFastFood;

            listaAttivita.Add(attivitaFastFood);


            //PRODOTTI TIPICI
            Attivita attivitaTipicalProduct = new Attivita();
            attivitaTipicalProduct.Sezione = Sezione.Product;
            attivitaTipicalProduct.Nome = "Le Bontà Del Borgo";
            attivitaTipicalProduct.Indirizzo = "Via Giuseppe Mazzini, 58, 26013 Crema CR";
            attivitaTipicalProduct.Telefono = "0373 86479";
            attivitaTipicalProduct.SitoWeb = "www.lebontadelborgo.it";
            attivitaTipicalProduct.Email = "info@lebontadelborgo.it";
            attivitaTipicalProduct.Descrizione = "";
            attivitaTipicalProduct.Latitudine = "45.3632711";
            attivitaTipicalProduct.Longitudine = "9.690130100000033";

            List<Immagine> listaImmaginiTipicalProduct = new List<Immagine>();

            listaImmaginiTipicalProduct.Add(new Immagine { ImageUrl = "foto_prodotti_1.jpg" });
            //listaImmaginiTipicalProduct.Add(new Immagine { ImageUrl = "foto_prodotti_2.jpg" });
            //listaImmaginiTipicalProduct.Add(new Immagine { ImageUrl = "foto_prodotti_3.jpg" });
            //listaImmaginiTipicalProduct.Add(new Immagine { ImageUrl = "foto_prodotti_4.jpg" });

            attivitaTipicalProduct.Immagini = listaImmaginiTipicalProduct;

            listaAttivita.Add(attivitaTipicalProduct);


            //CAR SERVICE
            Attivita attivitaCarService = new Attivita();
            attivitaCarService.Sezione = Sezione.Car;
            attivitaCarService.Nome = "Autofficina Corbellini";
            attivitaCarService.Indirizzo = "Via Dante Alighieri, 23, 26835 Crespiatica LO";
            attivitaCarService.Telefono = "0371 484466";
            attivitaCarService.SitoWeb = "-";
            attivitaCarService.Email = "-";
            attivitaCarService.Descrizione = "";
            attivitaCarService.Latitudine = "45.35464";
            attivitaCarService.Longitudine = "9.575849999999946";

            List<Immagine> listaImmaginiCarService = new List<Immagine>();

            listaImmaginiCarService.Add(new Immagine { ImageUrl = "foto_car_1.jpg" });
            //listaImmaginiCarService.Add(new Immagine { ImageUrl = "foto_car_2.jpg" });
            //listaImmaginiCarService.Add(new Immagine { ImageUrl = "foto_car_3.jpg" });
            //listaImmaginiCarService.Add(new Immagine { ImageUrl = "foto_car_4.jpg" });

            attivitaCarService.Immagini = listaImmaginiCarService;

            listaAttivita.Add(attivitaCarService);


            return listaAttivita;
        }




        public static List<Position> GetPosizioni(string nomeFile)
        {
            List<Position> listaPosizioni = new List<Position>();

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(nomeFile);

                foreach (XmlNode nod1 in doc.DocumentElement.ChildNodes)
                {
                    if (nod1.Name != "trk")
                        continue;
                    foreach (XmlNode nod2 in nod1.ChildNodes)
                    {
                        if (nod2.Name != "trkseg")
                            continue;
                        foreach (XmlNode trkptNode in nod2)
                        {
                            if (trkptNode.Name != "trkpt")
                                continue;

                            listaPosizioni.Add(new Position(Convert.ToDouble(trkptNode.Attributes[0].Value,CultureInfo.InvariantCulture),Convert.ToDouble(trkptNode.Attributes[1].Value,CultureInfo.InvariantCulture)));
                        }
                    }
                }

                return listaPosizioni;
            }
            catch { return listaPosizioni; }
        }
        

    }
}
