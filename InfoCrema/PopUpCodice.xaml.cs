﻿using System;
using System.Collections.Generic;
using System.Linq;
using InfoCrema.Models;
using InfoCrema.Utility;
using Xamarin.Forms;

namespace InfoCrema
{
    public partial class PopUpCodice : ContentView
    {
        public ListaTracciati pageContent { get; set; }
        public Attivita attivita { get; set; }

        public PopUpCodice()
        {
            InitializeComponent();

            btnSalvaCodice.Clicked += BtnSalvaCodice_Clicked;
            btnAnnullaCodice.Clicked += BtnAnnullaCodice_Clicked;

        }



        void BtnSalvaCodice_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(editCodice.Text) || attivita.Codici.FirstOrDefault(x => x.ToLower() == editCodice.Text.ToLower()) == null)
                pageContent.InserisciCodiceCompleted(false,"Il codice inserito non è valido",string.Empty);
            else
            {
                pageContent.InserisciCodiceCompleted(true, "Codice riscattato con successo", editCodice.Text);
                editCodice.Text = string.Empty;
                this.IsVisible = false;
            }
                
        }

        void BtnAnnullaCodice_Clicked(object sender, EventArgs e)
        {
            editCodice.Text = string.Empty;
            this.IsVisible = false;
        }



    }
}
