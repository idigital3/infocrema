﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace InfoCrema
{
    [ContentProperty("ContainerContent")]
    public partial class MasterPage : ContentView
    {
        public MasterPage()
        {
            InitializeComponent();

            var tapGesture = new Xamarin.Forms.TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;

            img_titolo.GestureRecognizers.Add(tapGesture);
            img_back.GestureRecognizers.Add(tapGesture);
        }


        public View ContainerContent
        {
            get { return ContentFrame.Content; }
            set { ContentFrame.Content = value; }
        }


        public bool HomeEnabled
        {
            get { return img_titolo.IsEnabled; }
            set { img_titolo.IsEnabled = value; }
        }

        public bool BackVisible
        {
            get { return img_back.IsVisible; }
            set { img_back.IsVisible = value; }

        }


        void TapGesture_Tapped(object sender, EventArgs e)
        {
            string nome_componente = sender.GetType().Name;

            if(nome_componente.Equals("Image"))
            {
                Image image = (Image)sender;
                string class_name = image.ClassId;

                if (class_name.Equals("img_back"))
                {
                    Navigation.PopAsync();
                }
                else if(class_name.Equals("img_titolo"))
                {
                    Navigation.PopToRootAsync();
                }
            }
        }

    }
}
